"""
=====================
Markov language model
=====================

Author: Haotian Shi, 2016

"""


import string
import random
from collections import deque
import math


############################################################
# Markov Models
############################################################

def tokenize(text):
    punctuations = set(string.punctuation)
    temp = []
    start = 0
    for i in range(len(text)):
        if text[i] in punctuations:
            if start < i:
                temp.append(text[start:i])
            temp.append(text[i])
            start = i + 1
    if start < len(text):
        temp.append(text[start:])
    tokens = [token for each in temp for token in each.split(' ') if token != '']
    return tokens
    

def ngrams(n, tokens):
    grams = []
    for i in range(len(tokens)+1):
        context = []
        j = i - (n - 1)
        while(j < i):
            if j < 0:
                context.append('<START>')
            else:
                context.append(tokens[j])
            j += 1
        if i < len(tokens):
            grams.append((tuple(context), tokens[i]))
        else:
            grams.append((tuple(context), '<END>'))
    return grams

class NgramModel(object):

    def __init__(self, n):
        self.n = n
        self.ngram = {}
        self.count = 0

    def update(self, sentence):
        grams = ngrams(self.n, tokenize(sentence))
        self.count += len(grams)
        for gram in grams:
            if gram[0] not in self.ngram:
                self.ngram[gram[0]] = [{gram[1]:1}, 1]
            else:
                if gram[1] in self.ngram[gram[0]][0]:
                    self.ngram[gram[0]][0][gram[1]] += 1
                else:
                    self.ngram[gram[0]][0][gram[1]] = 1
                self.ngram[gram[0]][1] += 1

    def prob(self, context, token):
        if context not in self.ngram or token not in self.ngram[context][0]:
            return float(0)
        return float(self.ngram[context][0][token]) / self.ngram[context][1]

    def random_token(self, context):
        if context not in self.ngram:
            return
        T = sorted(self.ngram[context][0])
        r = random.random()
        Psum = 0
        i = 0
        while(Psum <= r):
            Psum += self.prob(context, T[i])
            i += 1
        return T[i-1]
        

    def random_text(self, token_count):
        context = deque(['<START>' for i in range(self.n-1)])
        text = []
        while(token_count > 0):
            token = self.random_token(tuple(context))
            text.append(token)
            if token != '<END>':
                context.append(token)
                context.popleft()
            else:
                context = deque(['<START>' for i in range(self.n-1)])
            token_count -= 1
        return ' '.join(text)

    def perplexity(self, sentence):
        tokens = tokenize(sentence)
        context = deque(['<START>' for i in range(self.n-1)])
        logP = 0
        i = 0
        while(i < len(tokens)):
            logP += math.log(self.prob(tuple(context), tokens[i]))
            context.append(tokens[i])
            context.popleft()
            i += 1
        logP += math.log(self.prob(tuple(context), '<END>'))
        return math.e ** (-1.0 / (len(tokens) + 1) * logP)

def create_ngram_model(n, path):
    m = NgramModel(n)
    with open(path) as f:
        for eachLine in f:
            m.update(eachLine)
    return m


if __name__ == '__main__':
    m = NgramModel(1)
    m.update("a b c d")
    m.update("a b a b")
    print m.prob((), "a")
    print m.prob((), "c")
    print m.prob((), "<END>")

    m = NgramModel(2)
    m.update("a b c d")
    m.update("a b a b")
    print m.prob(("<START>",), "a")
    print m.prob(("b",), "c")
    print m.prob(("a",), "x")

    m = NgramModel(1)
    m.update("a b c d")
    m.update("a b a b")
    random.seed(1)
    print [m.random_token(()) for i in range(25)]

    m = NgramModel(2)
    m.update("a b c d")
    m.update("a b a b")
    random.seed(2)
    print [m.random_token(("<START>",)) for i in range(6)]
    print [m.random_token(("b",)) for i in range(6)]

    m = NgramModel(1)
    m.update("a b c d")
    m.update("a b a b")
    random.seed(1)
    print m.random_text(13)

    m = NgramModel(2)
    m.update("a b c d")
    m.update("a b a b")
    random.seed(2)
    print m.random_text(15)

    random.seed(1)
    m = create_ngram_model(1, "data\\frankenstein.txt")
    print m.random_text(15)

    m = NgramModel(1)
    m.update("a b c d")
    m.update("a b a b")
    print m.perplexity("a b")

    m = NgramModel(2)
    m.update("a b c d")
    m.update("a b a b")
    print m.perplexity("a b")

